# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_routes_session',
  :secret      => '5418158e0274b8b0e624b17e76a1bf45db574af69bc4450a36afced6abbb253c37b747e67fd0d5ab03fd901ca4c5978e277f1c8db13beaf2662f3c74fc60f506'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
