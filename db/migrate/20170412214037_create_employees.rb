class CreateEmployees < ActiveRecord::Migration
  def self.up
    create_table :employees do |t|
      t.column :first, :string
      t.column :last, :string
      t.timestamps
    end
  end

  def self.down
    drop_table :employees
  end
end
