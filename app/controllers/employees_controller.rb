class EmployeesController < ApplicationController
  def index
    @employees = Employee.find :all
  end

  def show
    @employee = Employee.find params[:id]
  end

  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new params[:employee]
    if @employee.save
      redirect_to :action => 'show', :id => @employee.id
    else
      render :action => 'new'
    end
  end

  def destroy
    @employee = Employee.find params[:id]
    @employee.destroy
  end

  def edit
    @employee = Employee.find params[:id]
  end

  def update
    @employee = Employee.find params[:id]
    if @employee.update_attributes(params[:employee])
      redirect_to :action => 'show', :id => @employee.id
    end
  end

  def shuffle
    @employees = Employee.find :all
  end
end
