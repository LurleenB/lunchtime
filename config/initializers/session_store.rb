# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_lunchtime_session',
  :secret      => '408f6652d1deb1d2134a5497e634dd4a1a7dd582eeffae18a15b429a9889c574c8509c9f61fce24fe9d07f674408bba6845ed90a042ab1ac6e77cef2156361b8'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
